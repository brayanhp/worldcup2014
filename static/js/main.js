var init = function(){

    /* Tabs */

    var itemContentMatch = $(".itemContentMatch");
    var elementTab = $('.contentDays li a');
    itemContentMatch.hide();
    elementTab.on('click', function() {

        elementTab.removeClass("active");
        $(this).addClass("active");
        itemContentMatch.hide();
        var activeTab = $(this).attr("href");
        $(activeTab).fadeIn();
        return false;
    });

    hash = '#tab1';
    elements = $('a[href="' + hash + '"]');
    if (elements.length === 0) {
        $(".contentDays li a:first").addClass("active").show();
        $(".itemContentMatch:first").show();
    } else {
        elements.click();
    }

    /* End Tabs */

    /* Clock */

    $('#countDays').countdown('2014/06/13', function(event) {
        $(this).html(event.strftime(' '+'<span class="numberClock">%D</span><span class="detailClock"> DIAS</span>' + '<span class="numberClock"> %H</span><span class="detailClock"> HORAS</span>' + '<span class="numberClock"> %M</span><span class="detailClock"> MINUTOS</span>'));
    });
}

init();